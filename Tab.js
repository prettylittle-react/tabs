import React from 'react';
import PropTypes from 'prop-types';

/**
 * Tab
 * @description [Description]
 * @example
  <div id="Tab"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Tab, {
        title : 'Example Tab'
    }), document.getElementById("Tab"));
  </script>
 */
class Tab extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'tab';
	}

	render() {
		const {children} = this.props;

		return children;
	}
}

Tab.defaultProps = {
	label: null,
	id: null,
	children: null
};

Tab.propTypes = {
	label: PropTypes.string.isRequired,
	id: PropTypes.string,
	children: PropTypes.node
};

export default Tab;
