import React from 'react';
import PropTypes from 'prop-types';

import Link from 'link';
import {getModifiers} from 'libs/component';
import {slugify} from 'libs/string';

import './Tabs.scss';

/**
 * Tabs
 * @description [Description]
 * @example
  <div id="Tabs"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Tabs, {
        title : 'Example Tabs'
    }), document.getElementById("Tabs"));
  </script>
 */
class Tabs extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'tabs';

		this.state = {
			activeIndex: props.selected
		};

		this.ids = {};
	}

	onClick = (ev, index) => {
		const {useHash} = this.props;

		if (!useHash) {
			ev.preventDefault();
			this.setState({activeIndex: index});
		}
	};

	onHashChange = ev => {
		this.hashCheck(ev.target.location.hash);
	};

	componentDidMount() {
		const {useHash} = this.props;
		this.hashCheck(window.location.hash, true);

		if (useHash) {
			window.addEventListener('hashchange', this.onHashChange, false);
		}
	}

	conponentWillUnmount() {
		const {useHash} = this.props;
		if (useHash) {
			window.removeEventListener('hashchange', this.onHashChange);
		}
	}

	hashCheck(hash, anchor = false) {
		if (hash) {
			const id = hash.substr(1);

			if (this.ids[id] !== undefined) {
				this.setState({activeIndex: this.ids[id]}, () => {
					if (anchor) {
						setTimeout(() => {
							this.component.scrollIntoView();
						}, 2000);
					}
				});
			}
		}
	}

	render() {
		const {isHorizontal, children} = this.props;

		const {activeIndex} = this.state;
		const className = getModifiers(this.baseClass, [isHorizontal ? 'horizontal' : 'vertical']);

		return (
			<div className={className} ref={component => (this.component = component)}>
				<div className={`${this.baseClass}__nav`}>
					<ul>
						{children.map((child, index) => {
							const {label, id} = child.props;
							const tabId = id || slugify(label);

							this.ids[tabId] = index;

							return (
								<li key={label}>
									<Link
										label={label}
										href={`#${tabId}`}
										onClick={ev => {
											this.onClick(ev, index);
										}}
										className={activeIndex === index ? 'active' : ''}
										baseClass=""
									/>
								</li>
							);
						})}
					</ul>
				</div>
				<div className={`${this.baseClass}__content`}>
					{children.map((child, index) => {
						if (activeIndex !== index) {
							return null;
						}
						// if (child.props.label !== activeTab) return undefined;
						return child.props.children;
					})}
				</div>
			</div>
		);
	}
}

Tabs.defaultProps = {
	selected: 0,
	useHash: true,
	isHorizontal: true,
	children: null
};

Tabs.propTypes = {
	selected: PropTypes.number,
	useHash: PropTypes.bool,
	isHorizontal: PropTypes.bool,
	children: PropTypes.node
};

export default Tabs;
